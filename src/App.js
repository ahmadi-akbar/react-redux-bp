import React from "react";
import { Provider } from "react-redux";
import store from "./actions/store";
import Screens from "./screens";

import { HashRouter as Router } from "react-router-dom";
import WebRoutesGenerator from "./components/NativeWebRouteWrapper";

const route = {
  Home: {
    component: Screens.Home,
    path: "/",
    exact: true
  }
};

function App() {
  return (
    <Router>
      <Provider store={store}>{WebRoutesGenerator(route)}</Provider>
    </Router>
  );
}

export default App;
