import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import reducer from "./reducer";
import { loadState, saveState } from "../components/Utils";
const persistedState = loadState();

let store;
if (process.env.NODE_ENV === "production")
  store = createStore(reducer, persistedState, applyMiddleware(thunk));
else
  store = createStore(reducer, persistedState, applyMiddleware(thunk, logger));

store.subscribe(() => {
  saveState({
    store: store.getState().store
  });
});

export default store;
