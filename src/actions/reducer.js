import { combineReducers } from "redux";

import Types from "./types";

const initialState = {};

const reducer = (state = initialState, action) => {
  const { type, data } = action;
  switch (type) {
    case Types.Home:
      return Object.assign({}, state, {
        data: {},
        loading: true,
        isError: false
      });

    case Types.Receive:
      return Object.assign({}, state, {
        data: data,
        loading: false,
        isError: false
      });
    case Types.Error:
      return Object.assign({}, state, {
        isError: true,
        loading: false
      });
    case Types.SaveData: {
      return Object.assign({}, state, data);
    }
    default:
      return state;
  }
};

export default combineReducers({
  store: reducer
});
