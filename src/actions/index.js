import Types from "./types";

export const Home = () => ({
  type: Types.Home
});

export const receive_data = data => ({
  type: Types.Receive,
  data: data
});

export const receive_error = data => ({
  type: Types.Error,
  data: data
});

export const SaveData = data => ({
  type: Types.SaveData,
  data: data
});
